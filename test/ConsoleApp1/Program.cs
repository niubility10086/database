﻿using Dapper;
using MySqlConnector;
using Niubility.Database;
using Niubility.Database.Entity;
using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Transactions;

namespace ConsoleApp1
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            //TestSqlite(@"niubility.db");
            //TestMySql(@"");
            //TestSqlServer(@"");
        }

        private static void TestSqlite(string databaseFile)
        {
            if (!File.Exists(databaseFile))
            {
                SQLiteConnection.CreateFile(databaseFile);
            }
            {
                using var conn = new SQLiteConnection($"Data Source={databaseFile};");
                var command = new SQLiteCommand(@"CREATE TABLE IF NOT EXISTS ""SystemConfigurations""(
	""Key"" VARCHAR(100) NOT NULL PRIMARY KEY,
	""Value"" VARCHAR(1000) NOT NULL
)", conn);
                conn.Open();
                command.ExecuteNonQuery();
            }
            var test = new DatabaseTest(SqlDialect.SqLite,
                () => new SQLiteConnection($"Data Source={databaseFile};"));
            test.Test();
        }
        private static void TestMySql(string connectionString)
        {
            var conn = new MySqlConnection(connectionString);
            using (conn)
            {
                conn.Execute(@"CREATE TABLE IF NOT EXISTS `SystemConfigurations` (
	`Key` VARCHAR(100) NOT NULL,
	`Value` VARCHAR(1000) NOT NULL,
	CONSTRAINT `SystemConfigurations_OK` PRIMARY KEY (`Key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;");
            };
            var test = new DatabaseTest(SqlDialect.MySql,
                () => new MySqlConnection(connectionString));
            test.Test();
        }
        private static void TestSqlServer(string connectionString)
        {
            var conn = new SqlConnection(connectionString);
            using (conn)
            {
                conn.Execute(@"IF OBJECT_ID('[dbo].[SystemConfigurations]', N'U') IS NULL BEGIN
	CREATE TABLE [dbo].[SystemConfigurations] (
		[Key] NVARCHAR(100) NOT NULL PRIMARY KEY,
		[Value] NVARCHAR(1000) NOT NULL
	) ON [PRIMARY]
END");
            };
            var test = new DatabaseTest(SqlDialect.SqlServer,
                () => new SqlConnection(connectionString));
            test.Test();
        }

        private class DatabaseTest
        {
            private static readonly SystemConfigurations[] TestEntities = new[] {
                new SystemConfigurations { Key = "ABC", Value = "Test" },
                new SystemConfigurations { Key = "123", Value = "Test2" }
            };
            private readonly SystemConfigurationRepository Repository;

            public DatabaseTest(SqlDialect sqlDialect,
                Func<IDbConnection> connection)
            {
                OrmConfiguration.SqlDialect = sqlDialect;
                Repository = new SystemConfigurationRepository(connection);
            }

            public void Test()
            {
                CheckEntities(TestEntities);

                Repository.InsertOrUpdateEntities(TestEntities);
                CheckEntities(TestEntities);

                Repository.DeleteEntitiesByPrimaryKeys(TestEntities);
                CheckEntities(TestEntities);
            }

            private void CheckEntities(params SystemConfigurations[] keys)
            {
                var entities = Repository.SelectEntitiesByPrimaryKeys(keys);
                if (entities.Any())
                {
                    foreach (var entity in entities)
                    {
                        Console.WriteLine("{0}: {1}", entity.Key, entity.Value);
                    }
                }
                else
                {
                    Console.WriteLine("<None>");
                }
                Console.WriteLine("==============================");
            }
        }

        private class SystemConfigurationRepository : NiubilityRepository<SystemConfigurations>
        {
            public SystemConfigurationRepository(Func<IDbConnection> connection)
                : base(new DatabaseConnectionManager(connection))
            { }
        }
        private class DatabaseConnectionManager : IDatabaseConnectionManager
        {
            private readonly Func<IDbConnection> Connection;

            public DatabaseConnectionManager(Func<IDbConnection> connection)
            {
                Connection = connection;
            }

            public IDbConnection GetDbConnection()
            {
                return Connection();
            }
        }
        [Table("", "dbo", "SystemConfigurations")]
        private class SystemConfigurations
        {
            [Column(false, false, false, true, 200, null, 1, null)]
            public string Key { get; set; }
            [Column(false, false, false, false, 2000, null, 2, null)]
            public string Value { get; set; }
        }
    }
}