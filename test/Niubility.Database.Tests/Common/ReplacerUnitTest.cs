﻿using Niubility.Database.Common;
using NUnit.Framework;

namespace Niubility.Database.Tests
{
    internal class ReplacerUnitTest
    {
        [Test]
        public void Test1()
        {
            var re = new Replacer(
                (@"^((?'S'\[)?(?<name>.+))*(?'E-S'\])?$(?(S)(?!))", "${name}")
            );
            TestApplyAllRules(re, "Keys", "Keys");
            TestApplyAllRules(re, "[Keys", "[Keys");
            TestApplyAllRules(re, "[Keys]", "Keys");
            TestApplyAllRules(re, "K[ey]s", "K[ey]s");
            TestApplyAllRules(re, "[K[ey]s]", "K[ey]s");
            TestApplyAllRules(re, "Keys]", "Keys]");
        }

        private void TestApplyAllRules(Replacer re, string word, string expected)
        {
            var actual = re.ApplyAllRules(word);
            Assert.That(actual, Is.EqualTo(expected));
        }
    }
}