﻿using Niubility.Database.Common;
using NUnit.Framework;

namespace Niubility.Database.Tests
{
    public class InflectorExtensionsUnitTest
    {
        /*
        [SetUp]
        public void Setup()
        {
        }
        */

        [Test]
        public void TestInflectorFunctions()
        {
            TestAll("dish_class", "DishClass", "dish_class", "dish_classes");
            TestAll("dish_classes", "DishClasses", "dish_class", "dish_classes");
            TestAll("dish_goods", "DishGoods", "dish_goods", "dish_goods");
            TestAll("dish_good", "DishGood", "dish_good", "dish_goods");
            TestAll("dish_histories", "DishHistories", "dish_history", "dish_histories");
            TestAll("tickets", "Tickets", "ticket", "tickets");
            TestAll("ApiPermits", "ApiPermits", "ApiPermit", "ApiPermits");
            TestAll("task_status", "TaskStatus", "task_status", "task_status");
        }

        private static void TestAll(string word,
            string expectedPascal, string expectedSingular, string expectedPlural)
        {
            TestPascalize(word, expectedPascal);
            TestSingularize(word, expectedSingular);
            TestPluralize(word, expectedPlural);
        }
        private static void TestPascalize(string word, string expected)
        {
            var actual = word.Pascalize();
            Assert.That(actual, Is.EqualTo(expected));
        }
        private static void TestSingularize(string word, string expected)
        {
            var actual = word.Singularize();
            Assert.That(actual, Is.EqualTo(expected));
        }
        private static void TestPluralize(string word, string expected)
        {
            var actual = word.Pluralize();
            Assert.That(actual, Is.EqualTo(expected));
        }
    }
}