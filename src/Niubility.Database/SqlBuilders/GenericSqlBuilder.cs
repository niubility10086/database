using Dapper;
using Niubility.Database.Configuration;
using Niubility.Database.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;

namespace Niubility.Database.SqlBuilders
{
    internal abstract class GenericSqlBuilder
    {
        protected readonly EntityRegistration EntityRegistration;
        protected readonly DatabaseOptions DatabaseOptions;

        protected readonly Lazy<string> FullTableName;
        protected readonly Lazy<string> TableAlias;

        protected readonly Lazy<string> SourceTableAlias;
        protected readonly Lazy<string> AllColumnsClause;
        protected readonly Lazy<string> PrimaryKeyColumnsClause;
        protected readonly Lazy<string> NonAIColumnsClause;
        protected readonly Lazy<string> PrimaryKeyColumnsWhereClause;
        protected readonly Lazy<string> NonAIColumnsUpdateClause;

        protected readonly Lazy<string> SelectEntitiesByPrimaryKeysStatementTemplate;
        protected readonly Lazy<string> SelectEntitieStatementTemplate;
        protected readonly Lazy<string> InsertOrUpdateStatementTemplate;
        protected readonly Lazy<string> DeleteEntitiesByPrimaryKeysStatementTemplate;

        protected GenericSqlBuilder(EntityRegistration entityRegistration,
            SqlDialect dialect)
        {
            EntityRegistration = entityRegistration;
            DatabaseOptions = GetDatabaseOptions(dialect);

            FullTableName = new Lazy<string>(() => GetTableName(EntityRegistration), LazyThreadSafetyMode.PublicationOnly);
            TableAlias = new Lazy<string>(() => QuoteIdentifier(EntityRegistration.TableName), LazyThreadSafetyMode.PublicationOnly);

            SourceTableAlias = new Lazy<string>(() => QuoteIdentifier("NiubilitySource"), LazyThreadSafetyMode.PublicationOnly);
            AllColumnsClause = new Lazy<string>(GenerateAllColumnsClause, LazyThreadSafetyMode.PublicationOnly);
            PrimaryKeyColumnsClause = new Lazy<string>(GeneratePrimaryKeyColumnsClause, LazyThreadSafetyMode.PublicationOnly);
            NonAIColumnsClause = new Lazy<string>(GenerateNonAIColumnsClause, LazyThreadSafetyMode.PublicationOnly);
            PrimaryKeyColumnsWhereClause = new Lazy<string>(GeneratePrimaryKeyColumnsWhereClause, LazyThreadSafetyMode.PublicationOnly);
            NonAIColumnsUpdateClause = new Lazy<string>(GenerateNonAIColumnsUpdateClause, LazyThreadSafetyMode.PublicationOnly);

            SelectEntitiesByPrimaryKeysStatementTemplate = new Lazy<string>(GenerateSelectEntitiesByPrimaryKeysStatementTemplateInternal, LazyThreadSafetyMode.PublicationOnly);
            SelectEntitieStatementTemplate = new Lazy<string>(GenerateSelectEntitieStatementTemplateInternal, LazyThreadSafetyMode.PublicationOnly);
            InsertOrUpdateStatementTemplate = new Lazy<string>(GenerateInsertOrUpdateStatementTemplateInternal, LazyThreadSafetyMode.PublicationOnly);
            DeleteEntitiesByPrimaryKeysStatementTemplate = new Lazy<string>(GenerateDeleteEntitiesByPrimaryKeysStatementTemplateInternal, LazyThreadSafetyMode.PublicationOnly);
        }

        protected virtual string GenerateAllColumnsClause()
        {
            var columns = EntityRegistration.Properties
                .Select(k => QuoteIdentifier(k.PropertyInfo.Name)).ToArray();
            return string.Join(",", columns);
        }
        protected virtual string GeneratePrimaryKeyColumnsClause()
        {
            var columns = EntityRegistration.Properties.Where(p => p.ColumnAttribute.IsPrimaryKey)
                .Select(k => QuoteIdentifier(k.PropertyInfo.Name)).ToArray();
            return string.Join(",", columns);
        }
        protected virtual string GenerateNonAIColumnsClause()
        {
            var columns = EntityRegistration.Properties.Where(p => !p.ColumnAttribute.IsAutoIncrement)
                .Select(k => QuoteIdentifier(k.PropertyInfo.Name)).ToArray();
            return string.Join(",", columns);
        }
        protected virtual string GeneratePrimaryKeyColumnsWhereClause()
        {
            var columns = EntityRegistration.Properties.Where(p => p.ColumnAttribute.IsPrimaryKey)
                .Select(k => QuoteIdentifier(k.PropertyInfo.Name)).ToArray();
            return string.Join(" AND ", columns.Select(col => string.Format(@"({0}.{2} = {1}.{2})",
                    TableAlias.Value, SourceTableAlias.Value,
                    col)));
        }
        protected virtual string GenerateNonAIColumnsUpdateClause()
        {
            var nonAIKeys = EntityRegistration.Properties.Where(p => !p.ColumnAttribute.IsAutoIncrement)
                .Select(k => QuoteIdentifier(k.PropertyInfo.Name)).ToArray();
            return string.Join(",\r\n", nonAIKeys.Select(k => $"\t{TableAlias.Value}.{k} = {SourceTableAlias.Value}.{k}"));
        }

        protected abstract string GenerateReadLockClause(
            ReadLockTypes readLock, BehaviorsOnBlocked behaviorsOnBlocked);
        protected abstract string[] GenerateValueClauses<TEntity>(IEnumerable<TEntity> data,
            IEnumerable<PropertyRegistration> properties,
            DynamicParameters parameters);
        protected virtual string JoinValueClauses(IEnumerable<string> valueClauses)
        {
            return string.Join(",\r\n", valueClauses);
        }


        public virtual (string, DynamicParameters) GenerateSelectEntitiesByPrimaryKeysSqlAndParameters<TEntity>(
            IEnumerable<TEntity> keys,
            ReadLockTypes readLock, BehaviorsOnBlocked behaviorsOnBlocked)
        {
            var parameters = new DynamicParameters();
            var primaryKeys = EntityRegistration.Properties.Where(p => p.ColumnAttribute.IsPrimaryKey);
            var values = GenerateValueClauses(keys, primaryKeys, parameters);
            var sql = string.Format(SelectEntitiesByPrimaryKeysStatementTemplate.Value,
                GenerateReadLockClause(readLock, behaviorsOnBlocked),
                JoinValueClauses(values));
            return (sql, parameters);
        }
        protected abstract string GenerateSelectEntitiesByPrimaryKeysStatementTemplateInternal();

        public virtual (string, DynamicParameters) GenerateSelectEntitiesSqlAndParameters(
            ReadLockTypes readLock, BehaviorsOnBlocked behaviorsOnBlocked)
        {
            var sql = string.Format(SelectEntitieStatementTemplate.Value,
                GenerateReadLockClause(readLock, behaviorsOnBlocked));
            return (sql, null);
        }
        protected abstract string GenerateSelectEntitieStatementTemplateInternal();

        public virtual (string, DynamicParameters) GenerateInsertOrUpdateSqlAndParameters<TEntity>(
            IEnumerable<TEntity> entities)
        {
            var parameters = new DynamicParameters();
            var values = GenerateValueClauses(entities, EntityRegistration.Properties, parameters);
            var sql = string.Format(InsertOrUpdateStatementTemplate.Value,
                JoinValueClauses(values));
            return (sql, parameters);
        }
        protected abstract string GenerateInsertOrUpdateStatementTemplateInternal();

        public virtual (string, DynamicParameters) GenerateDeleteEntitiesByPrimaryKeysSqlAndParameters<TEntity>(
            IEnumerable<TEntity> keys)
        {
            var parameters = new DynamicParameters();
            var primaryKeys = EntityRegistration.Properties.Where(p => p.ColumnAttribute.IsPrimaryKey);
            var values = GenerateValueClauses(keys, primaryKeys, parameters);
            var sql = string.Format(DeleteEntitiesByPrimaryKeysStatementTemplate.Value,
                JoinValueClauses(values));
            return (sql, parameters);
        }
        protected abstract string GenerateDeleteEntitiesByPrimaryKeysStatementTemplateInternal();



        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public virtual string QuoteIdentifier(string identifier)
        {
            return DatabaseOptions.QuoteIdentifier(identifier);
        }
        public virtual string QuoteParameter(string parameter)
        {
            return string.Concat(DatabaseOptions.ParameterPrefix, parameter, DatabaseOptions.ParameterSuffix);
        }
        public virtual string GetTableName(EntityRegistration entity)
        {
            var tableName = QuoteIdentifier(entity.TableName);
            if (DatabaseOptions.IsUsingSchemas
                && (!string.IsNullOrEmpty(entity.SchemaName)))
            {
                var schemaName = QuoteIdentifier(entity.SchemaName);
                tableName = string.Format("{0}.{1}", schemaName, tableName);
            }
            return tableName;
        }

        private static readonly DatabaseOptions DefaultSqlServerDatabaseOptions = new SqlServerDatabaseOptions();
        private static readonly DatabaseOptions DefaultMySqlDatabaseOptions = new MySqlDatabaseOptions();
        private static readonly DatabaseOptions DefaultGenericDatabaseOptions = new DatabaseOptions();
        public virtual DatabaseOptions GetDatabaseOptions(SqlDialect dialect)
        {
            switch (dialect)
            {
                case SqlDialect.SqlServer:
                    return DefaultSqlServerDatabaseOptions;
                case SqlDialect.MySql:
                    return DefaultMySqlDatabaseOptions;
                default:
                    return DefaultGenericDatabaseOptions;
            }
        }
    }
}