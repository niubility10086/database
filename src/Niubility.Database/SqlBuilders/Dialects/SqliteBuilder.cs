﻿using Dapper;
using Niubility.Database.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using static Dapper.SqlMapper;

namespace Niubility.Database.SqlBuilders
{
    internal class SqliteBuilder : GenericSqlBuilder
    {
        public SqliteBuilder(EntityRegistration entityRegistration)
            : base(entityRegistration, SqlDialect.SqLite)
        { }

        protected override string GenerateNonAIColumnsUpdateClause()
        {
            var nonAIKeys = EntityRegistration.Properties.Where(p => !p.ColumnAttribute.IsAutoIncrement)
                .Select(k => QuoteIdentifier(k.PropertyInfo.Name)).ToArray();
            return string.Join(",\r\n", nonAIKeys.Select(k => $"\t{k} = excluded.{k}"));
        }

        protected override string GenerateReadLockClause(
            ReadLockTypes readLock, BehaviorsOnBlocked behaviorsOnBlocked)
        {
            return string.Empty;
        }

        protected override string[] GenerateValueClauses<TEntity>(IEnumerable<TEntity> data,
            IEnumerable<PropertyRegistration> properties,
            DynamicParameters parameters)
        {
            return data.Select((d, i) => string.Format("\tSELECT {0}",
                string.Join(",", properties.Select(prop =>
                {
                    var pi = prop.PropertyInfo;
                    var name = $"{pi.Name}{i}";
                    var value = pi.GetValue(d, null);
                    parameters.Add(name, value);
                    return $"{QuoteParameter(name)} AS {QuoteIdentifier(pi.Name)}";
                })))).ToArray();
        }
        protected override string JoinValueClauses(IEnumerable<string> valueClauses)
        {
            return string.Join("\r\nUNION ALL\r\n", valueClauses);
        }


        protected override string GenerateSelectEntitiesByPrimaryKeysStatementTemplateInternal()
        {
            return string.Format(@"SELECT * FROM {0}
INNER JOIN (
{{1}}
) AS {1}
	ON {2}",
                FullTableName.Value,
                SourceTableAlias.Value, PrimaryKeyColumnsWhereClause.Value);
        }

        protected override string GenerateSelectEntitieStatementTemplateInternal()
        {
            return string.Format(@"SELECT * FROM {0}", FullTableName.Value);
        }

        public override (string, DynamicParameters) GenerateInsertOrUpdateSqlAndParameters<TEntity>(
            IEnumerable<TEntity> entities)
        {
            var parameters = new DynamicParameters();
            var sql = string.Format(@"INSERT INTO {0} ({1})
VALUES
{4}
ON CONFLICT ({2}) DO UPDATE SET
{3}",
                FullTableName.Value, AllColumnsClause.Value,
                PrimaryKeyColumnsClause.Value,
                NonAIColumnsUpdateClause.Value,
                string.Join(",\r\n", entities.Select((entity, i) => string.Format(@"({0})", string.Join(",",
                    EntityRegistration.Properties.Select(prop =>
                    {
                        var pi = prop.PropertyInfo;
                        var name = $"{pi.Name}{i}";
                        var value = pi.GetValue(entity, null);
                        parameters.Add(name, value);
                        return QuoteParameter(name);
                    }))))));
            return (sql, parameters);
        }
        protected override string GenerateInsertOrUpdateStatementTemplateInternal()
        {
            ////INSERT INTO Members(id, name) VALUES ($id, $name) ON CONFLICT (id) DO UPDATE SET name=$name;
            return null;
        }

        public override (string, DynamicParameters) GenerateDeleteEntitiesByPrimaryKeysSqlAndParameters<TEntity>(
            IEnumerable<TEntity> keys)
        {
            var parameters = new DynamicParameters();
            var primaryKeys = EntityRegistration.Properties.Where(p => p.ColumnAttribute.IsPrimaryKey);
            var conditions = keys.Select((key, i) => string.Format(@"({0})", string.Join(" AND ", primaryKeys.Select(pk =>
            {
                var pi = pk.PropertyInfo;
                var name = $"{pi.Name}{i}";
                var value = pi.GetValue(key, null);
                parameters.Add(name, value);
                return $"{QuoteIdentifier(pi.Name)} = {QuoteParameter(name)}";
            }))));
            var sql = string.Format(DeleteEntitiesByPrimaryKeysStatementTemplate.Value,
                string.Join(" OR ", conditions));
            return (sql, parameters);
        }
        protected override string GenerateDeleteEntitiesByPrimaryKeysStatementTemplateInternal()
        {
            return string.Format(@"DELETE FROM {0}
WHERE {{0}}",
                FullTableName.Value);
        }
    }
}