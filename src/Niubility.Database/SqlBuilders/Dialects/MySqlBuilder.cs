﻿using Dapper;
using Niubility.Database.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

namespace Niubility.Database.SqlBuilders
{
    internal class MySqlBuilder : GenericSqlBuilder
    {
        public MySqlBuilder(EntityRegistration entityRegistration)
            : base(entityRegistration, SqlDialect.MySql)
        { }

        protected override string GenerateNonAIColumnsUpdateClause()
        {
            var primaryKeys = EntityRegistration.Properties.Where(p => p.ColumnAttribute.IsPrimaryKey)
                .Select(k => QuoteIdentifier(k.PropertyInfo.Name)).ToArray();
            var nonAIKeys = EntityRegistration.Properties.Where(p => !p.ColumnAttribute.IsAutoIncrement)
                .Select(k => QuoteIdentifier(k.PropertyInfo.Name)).ToArray();
            var updateColumns = nonAIKeys.Except(primaryKeys);
            if (!updateColumns.Any())
            {
                return null;
            }
            return string.Join(",\r\n", updateColumns.Select(c => $"\t{c} = {SourceTableAlias.Value}.{c}"));
        }

        protected override string GenerateReadLockClause(
            ReadLockTypes readLock, BehaviorsOnBlocked behaviorsOnBlocked)
        {
            var hintReadLock = GetHint(readLock);
            if (!string.IsNullOrEmpty(hintReadLock))
            {
                var snippet = $" FOR {hintReadLock} OF {TableAlias.Value}";
                var hintBehaviorsOnBlocked = GetHint(behaviorsOnBlocked);
                if (!string.IsNullOrEmpty(hintBehaviorsOnBlocked))
                {
                    snippet = string.Concat(snippet, " ", hintBehaviorsOnBlocked);
                }
                return snippet;
            }
            return string.Empty;
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static string GetHint(ReadLockTypes readLock)
        {
            switch (readLock)
            {
                case ReadLockTypes.Share:
                    return "SHARE";
                case ReadLockTypes.Update:
                    return "UPDATE";
                case ReadLockTypes.None:
                default:
                    return null;
            }
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static string GetHint(BehaviorsOnBlocked behaviors)
        {
            switch (behaviors)
            {
                case BehaviorsOnBlocked.NoWait:
                    return "NOWAIT";
                case BehaviorsOnBlocked.SkipLocked:
                    return "SKIP LOCKED";
                case BehaviorsOnBlocked.Wait:
                default:
                    return null;
            }
        }

        protected override string[] GenerateValueClauses<TEntity>(IEnumerable<TEntity> data,
            IEnumerable<PropertyRegistration> properties,
            DynamicParameters parameters)
        {
            return data.Select((d, i) => string.Format("\tROW({0})",
                string.Join(",", properties.Select(prop =>
                {
                    var pi = prop.PropertyInfo;
                    var name = $"{pi.Name}{i}";
                    var value = pi.GetValue(d, null);
                    parameters.Add(name, value);
                    return QuoteParameter(name);
                })))).ToArray();
        }

        protected override string GenerateSelectEntitiesByPrimaryKeysStatementTemplateInternal()
        {
            return string.Format(@"SELECT * FROM {0}
INNER JOIN (
	VALUES
{{1}}
) AS {1} ({2})
	ON {3}
{{0}}",
                FullTableName.Value,
                SourceTableAlias.Value, PrimaryKeyColumnsClause.Value, PrimaryKeyColumnsWhereClause.Value);
        }

        protected override string GenerateSelectEntitieStatementTemplateInternal()
        {
            return string.Format(@"SELECT * FROM {0}{{0}}", FullTableName.Value);
        }

        protected override string GenerateInsertOrUpdateStatementTemplateInternal()
        {
            var updateClause = NonAIColumnsUpdateClause.Value;
            var shouldUpdate = null != updateClause;
            return string.Format(@"INSERT{1} INTO {0} ({2})
SELECT {2} FROM (
	VALUES {{0}}
) AS {3} ({4}){5}",
                FullTableName.Value,
                shouldUpdate ? string.Empty : " IGNORE",
                NonAIColumnsClause.Value,
                SourceTableAlias.Value,
                AllColumnsClause.Value,
                shouldUpdate
                    ? string.Format(@"
ON DUPLICATE KEY UPDATE
{0}",
                        NonAIColumnsUpdateClause.Value)
                    : string.Empty);
        }

        protected override string GenerateDeleteEntitiesByPrimaryKeysStatementTemplateInternal()
        {
            return string.Format(@"DELETE {1}
FROM {0}
INNER JOIN (
	VALUES
{{0}}
) AS {2} ({3})
	ON {4}",
                FullTableName.Value, TableAlias.Value,
                SourceTableAlias.Value, PrimaryKeyColumnsClause.Value, PrimaryKeyColumnsWhereClause.Value);
        }
    }
}