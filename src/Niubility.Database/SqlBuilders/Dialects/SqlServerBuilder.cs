﻿using Dapper;
using Niubility.Database.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

namespace Niubility.Database.SqlBuilders
{
    internal class SqlServerBuilder : GenericSqlBuilder
    {
        public SqlServerBuilder(EntityRegistration entityRegistration)
            : base(entityRegistration, SqlDialect.SqlServer)
        { }

        protected override string GenerateReadLockClause(
            ReadLockTypes readLock, BehaviorsOnBlocked behaviorsOnBlocked)
        {
            var hints = (new[] {
                GetHint(readLock),
                GetHint(behaviorsOnBlocked)
            }).Where(h => !string.IsNullOrEmpty(h));
            if (hints.Any())
            {
                return $" WITH ({string.Join(" ", hints)})";
            }
            return string.Empty;
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static string GetHint(ReadLockTypes readLock)
        {
            switch (readLock)
            {
                case ReadLockTypes.NoLock:
                    return "NOLOCK";
                case ReadLockTypes.Share:
                    return "ROWLOCK";
                case ReadLockTypes.Update:
                    return "UPDLOCK";
                case ReadLockTypes.None:
                default:
                    return null;
            }
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static string GetHint(BehaviorsOnBlocked behaviors)
        {
            switch (behaviors)
            {
                case BehaviorsOnBlocked.NoWait:
                    return "NOWAIT";
                case BehaviorsOnBlocked.SkipLocked:
                    return "READPAST";
                case BehaviorsOnBlocked.Wait:
                default:
                    return null;
            }
        }

        protected override string[] GenerateValueClauses<TEntity>(IEnumerable<TEntity> data,
            IEnumerable<PropertyRegistration> properties,
            DynamicParameters parameters)
        {
            return data.Select((d, i) => string.Format("\t({0})",
                string.Join(",", properties.Select(prop =>
                {
                    var pi = prop.PropertyInfo;
                    var name = $"{pi.Name}{i}";
                    var value = pi.GetValue(d, null);
                    parameters.Add(name, value);
                    return QuoteParameter(name);
                })))).ToArray();
        }

        protected override string GenerateSelectEntitiesByPrimaryKeysStatementTemplateInternal()
        {
            return string.Format(@"SELECT * FROM {0}{{0}}
INNER JOIN (
	VALUES
{{1}}
) AS {1} ({2})
	ON {3}",
                FullTableName.Value,
                SourceTableAlias.Value, PrimaryKeyColumnsClause.Value, PrimaryKeyColumnsWhereClause.Value);
        }

        protected override string GenerateSelectEntitieStatementTemplateInternal()
        {
            return string.Format(@"SELECT * FROM {0}{{0}}", FullTableName.Value);
        }

        protected override string GenerateInsertOrUpdateStatementTemplateInternal()
        {
            return string.Format(@"MERGE INTO {0}
USING (
	VALUES
{{0}}
) AS {1} ({2})
	ON {3}
WHEN MATCHED THEN UPDATE SET
{4}
WHEN NOT MATCHED BY TARGET THEN
	INSERT ({5})
	VALUES ({5});",
                FullTableName.Value, SourceTableAlias.Value,
                AllColumnsClause.Value,
                PrimaryKeyColumnsWhereClause.Value,
                NonAIColumnsUpdateClause.Value,
                NonAIColumnsClause.Value);
        }

        protected override string GenerateDeleteEntitiesByPrimaryKeysStatementTemplateInternal()
        {
            return string.Format(@"DELETE {1}
FROM {0}
INNER JOIN (
	VALUES
{{0}}
) AS {2} ({3})
	ON {4}",
                FullTableName.Value, TableAlias.Value,
                SourceTableAlias.Value, PrimaryKeyColumnsClause.Value, PrimaryKeyColumnsWhereClause.Value);
        }
    }
}