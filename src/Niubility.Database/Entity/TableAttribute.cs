﻿using System;

namespace Niubility.Database.Entity
{
    public class TableAttribute : Attribute
    {
        public string DatabaseName { get; }
        public string SchemaName { get; }
        public string TableName { get; }

        public TableAttribute(string database, string schema, string table)
        {
            DatabaseName = database;
            SchemaName = schema;
            TableName = table;
        }
        public TableAttribute(string schema, string table)
            : this(null, schema, table)
        { }
        public TableAttribute(string table)
            : this(null, null, table)
        { }
    }
}