﻿using Dapper;
using Niubility.Database.SqlBuilders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Niubility.Database.Entity
{
    internal class EntityDescriptor
    {
        private readonly EntityRegistration EntityRegistration;
        private readonly SqlDialect SqlDialect;
        private readonly Lazy<GenericSqlBuilder> GenericSqlBuilder;

        private EntityDescriptor(EntityRegistration entityRegistration,
            SqlDialect sqlDialect)
        {
            EntityRegistration = entityRegistration;
            SqlDialect = sqlDialect;
            GenericSqlBuilder = new Lazy<GenericSqlBuilder>(() => GenerateSqlBuilder(EntityRegistration, SqlDialect));
        }

        public (string, DynamicParameters) GenerateSelectEntitiesByPrimaryKeysSqlAndParameters<TEntity>(
            IEnumerable<TEntity> keys,
            ReadLockTypes readLock, BehaviorsOnBlocked behaviorsOnBlocked)
        {
            return GenericSqlBuilder.Value.GenerateSelectEntitiesByPrimaryKeysSqlAndParameters(keys,
                readLock, behaviorsOnBlocked);
        }
        public (string, DynamicParameters) GenerateSelectEntitiesSqlAndParameters(
            ReadLockTypes readLock, BehaviorsOnBlocked behaviorsOnBlocked)
        {
            return GenericSqlBuilder.Value.GenerateSelectEntitiesSqlAndParameters(
                readLock, behaviorsOnBlocked);
        }
        public (string, DynamicParameters) GenerateInsertOrUpdateSqlAndParameters<TEntity>(
            IEnumerable<TEntity> entities)
        {
            return GenericSqlBuilder.Value.GenerateInsertOrUpdateSqlAndParameters(entities);
        }
        public (string, DynamicParameters) GenerateDeleteEntitiesByPrimaryKeysSqlAndParameters<TEntity>(
            IEnumerable<TEntity> keys)
        {
            return GenericSqlBuilder.Value.GenerateDeleteEntitiesByPrimaryKeysSqlAndParameters(keys);
        }

        public static EntityDescriptor GenerateEntityDescriptor<TEntity>(SqlDialect sqlDialect)
        {
            var entityType = typeof(TEntity);
            var tableAttribute = entityType.GetCustomAttribute<TableAttribute>();
            if (null == tableAttribute)
            {
                return null;
            }
            var properties = entityType.GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.GetProperty)
                .Select(pi =>
                {
                    var columnAttribute = pi.GetCustomAttribute<ColumnAttribute>(true);
                    if (null == columnAttribute)
                    {
                        return null;
                    }
                    return new PropertyRegistration(pi, columnAttribute);
                }).Where(p => null != p);

            var entityRegistration = new EntityRegistration(
                tableAttribute.DatabaseName, tableAttribute.SchemaName, tableAttribute.TableName,
                properties);
            return new EntityDescriptor(entityRegistration, sqlDialect);
        }

        private static GenericSqlBuilder GenerateSqlBuilder(EntityRegistration entityRegistration, SqlDialect sqlDialect)
        {
            switch (sqlDialect)
            {
                case SqlDialect.SqLite:
                    return new SqliteBuilder(entityRegistration);
                case SqlDialect.SqlServer:
                    return new SqlServerBuilder(entityRegistration);
                case SqlDialect.MySql:
                    return new MySqlBuilder(entityRegistration);
                default:
                    throw new NotSupportedException($"Dialect {sqlDialect} is not supported");
            }
        }
    }
}