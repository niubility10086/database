﻿using System;
using System.Reflection;

namespace Niubility.Database.Entity
{
    public class PropertyRegistration
    {
        public PropertyInfo PropertyInfo { get; }
        public ColumnAttribute ColumnAttribute { get; }

        public PropertyRegistration(PropertyInfo info, ColumnAttribute columnAttribute)
        {
            PropertyInfo = info;
            ColumnAttribute = columnAttribute;
        }
    }
}