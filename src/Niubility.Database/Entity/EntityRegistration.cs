﻿using System.Collections.Generic;
using System.Linq;

namespace Niubility.Database.Entity
{
    internal class EntityRegistration
    {
        public string DatabaseName { get; }
        public string SchemaName { get; }
        public string TableName { get; }
        public PropertyRegistration[] Properties { get; }

        public EntityRegistration(
            string databaseName, string schemaName, string tableName,
            IEnumerable<PropertyRegistration> properties)
        {
            DatabaseName = databaseName;
            SchemaName = schemaName;
            TableName = tableName;
            Properties = properties.ToArray();
        }
    }
}