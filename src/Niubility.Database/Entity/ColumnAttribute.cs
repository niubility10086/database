﻿using System;

namespace Niubility.Database.Entity
{
    public class ColumnAttribute : Attribute
    {
        public int CharacterMaxLength { get; }
        public bool Nullable { get; }
        public bool HasDefault { get; }
        public bool IsAutoIncrement { get; }
        public bool IsPrimaryKey { get; }

        public string DefaultValue { get; }

        public int Ordinal { get; }
        public string Comment { get; }

        public ColumnAttribute(bool nullable, bool hasDefault,
            bool isAutoIncrement, bool isPrimaryKey,
            int charMaxLength, string defaultValue,
            int ordinal, string comment)
        {
            Nullable = nullable;
            HasDefault = hasDefault;
            IsAutoIncrement = isAutoIncrement;
            IsPrimaryKey = isPrimaryKey;
            CharacterMaxLength = charMaxLength;
            DefaultValue = defaultValue;
            Ordinal = ordinal;
            Comment = comment;
        }
    }
}