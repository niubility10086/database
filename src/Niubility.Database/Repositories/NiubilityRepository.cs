﻿using Dapper;
using Niubility.Database.Entity;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using static Dapper.SqlMapper;

namespace Niubility.Database
{
    public abstract class NiubilityRepository<TEntity> : INiubilityRepository
    {
        private readonly IDatabaseConnectionManager DatabaseConnectionManager;
        private readonly EntityDescriptor EntityDescriptor;

        protected NiubilityRepository(IDatabaseConnectionManager connectionManagement)
        {
            DatabaseConnectionManager = connectionManagement;
            EntityDescriptor = OrmConfiguration.GetEntityDescriptor<TEntity>();
        }

        protected IDbConnection GetDbConnection()
        {
            return DatabaseConnectionManager.GetDbConnection();
        }


        public TEntity SelectEntityByPrimaryKey(TEntity key,
            IDbConnection connection = null, IDbTransaction transaction = null,
            ReadLockTypes readLock = ReadLockTypes.None, BehaviorsOnBlocked behaviorsOnBlocked = BehaviorsOnBlocked.Wait)
        {
            if (null == connection)
            {
                using (var connection2 = GetDbConnection())
                {
                    return InternalSelectEntityByPrimaryKey(key, connection2, null, readLock, behaviorsOnBlocked);
                }
            }
            return InternalSelectEntityByPrimaryKey(key, connection, transaction, readLock, behaviorsOnBlocked);
        }
        public async Task<TEntity> SelectEntityByPrimaryKeyAsync(TEntity key,
            IDbConnection connection = null, IDbTransaction transaction = null,
            ReadLockTypes readLock = ReadLockTypes.None, BehaviorsOnBlocked behaviorsOnBlocked = BehaviorsOnBlocked.Wait)
        {
            if (null == connection)
            {
                using (var connection2 = GetDbConnection())
                {
                    return await InternalSelectEntityByPrimaryKeyAsync(key, connection2, null, readLock, behaviorsOnBlocked);
                }
            }
            return await InternalSelectEntityByPrimaryKeyAsync(key, connection, transaction, readLock, behaviorsOnBlocked);
        }
        public TEntity[] SelectEntitiesByPrimaryKeys(IEnumerable<TEntity> keys,
            IDbConnection connection = null, IDbTransaction transaction = null,
            ReadLockTypes readLock = ReadLockTypes.None, BehaviorsOnBlocked behaviorsOnBlocked = BehaviorsOnBlocked.Wait)
        {
            if (null == connection)
            {
                using (var connection2 = GetDbConnection())
                {
                    return InternalSelectEntitiesByPrimaryKeys(keys, connection2, null, readLock, behaviorsOnBlocked);
                }
            }
            return InternalSelectEntitiesByPrimaryKeys(keys, connection, transaction, readLock, behaviorsOnBlocked);
        }
        public async Task<TEntity[]> SelectEntitiesByPrimaryKeysAsync(IEnumerable<TEntity> keys,
            IDbConnection connection = null, IDbTransaction transaction = null,
            ReadLockTypes readLock = ReadLockTypes.None, BehaviorsOnBlocked behaviorsOnBlocked = BehaviorsOnBlocked.Wait)
        {
            if (null == connection)
            {
                using (var connection2 = GetDbConnection())
                {
                    return await InternalSelectEntitiesByPrimaryKeysAsync(keys, connection2, null, readLock, behaviorsOnBlocked);
                }
            }
            return await InternalSelectEntitiesByPrimaryKeysAsync(keys, connection, transaction, readLock, behaviorsOnBlocked);
        }
        private TEntity InternalSelectEntityByPrimaryKey(TEntity key,
            IDbConnection connection, IDbTransaction transaction,
            ReadLockTypes readLock = ReadLockTypes.None, BehaviorsOnBlocked behaviorsOnBlocked = BehaviorsOnBlocked.Wait)
        {
            var entities = InternalSelectEntitiesByPrimaryKeys(new[] { key },
                connection, transaction,
                readLock, behaviorsOnBlocked);
            return entities.FirstOrDefault();
        }
        private async Task<TEntity> InternalSelectEntityByPrimaryKeyAsync(TEntity key,
            IDbConnection connection, IDbTransaction transaction,
            ReadLockTypes readLock = ReadLockTypes.None, BehaviorsOnBlocked behaviorsOnBlocked = BehaviorsOnBlocked.Wait)
        {
            var entities = await InternalSelectEntitiesByPrimaryKeysAsync(new[] { key },
                connection, transaction,
                readLock, behaviorsOnBlocked);
            return entities.FirstOrDefault();
        }
        private TEntity[] InternalSelectEntitiesByPrimaryKeys(IEnumerable<TEntity> keys,
            IDbConnection connection, IDbTransaction transaction,
            ReadLockTypes readLock = ReadLockTypes.None, BehaviorsOnBlocked behaviorsOnBlocked = BehaviorsOnBlocked.Wait)
        {
            var (sql, parameters) = EntityDescriptor.GenerateSelectEntitiesByPrimaryKeysSqlAndParameters(keys,
                readLock, behaviorsOnBlocked);
            var entities = connection.Query<TEntity>(sql, parameters, transaction);
            return entities.ToArray();
        }
        private async Task<TEntity[]> InternalSelectEntitiesByPrimaryKeysAsync(IEnumerable<TEntity> keys,
            IDbConnection connection, IDbTransaction transaction,
            ReadLockTypes readLock = ReadLockTypes.None, BehaviorsOnBlocked behaviorsOnBlocked = BehaviorsOnBlocked.Wait)
        {
            var (sql, parameters) = EntityDescriptor.GenerateSelectEntitiesByPrimaryKeysSqlAndParameters(keys,
                readLock, behaviorsOnBlocked);
            var entities = await connection.QueryAsync<TEntity>(sql, parameters, transaction);
            return entities.ToArray();
        }

        public TEntity[] SelectEntities(
            IDbConnection connection = null, IDbTransaction transaction = null,
            ReadLockTypes readLock = ReadLockTypes.None, BehaviorsOnBlocked behaviorsOnBlocked = BehaviorsOnBlocked.Wait)
        {
            if (null == connection)
            {
                using (var connection2 = GetDbConnection())
                {
                    return InternalSelectEntities(connection2, null, readLock, behaviorsOnBlocked);
                }
            }
            return InternalSelectEntities(connection, transaction, readLock, behaviorsOnBlocked);
        }
        public async Task<TEntity[]> SelectEntitiesAsync(
            IDbConnection connection = null, IDbTransaction transaction = null,
            ReadLockTypes readLock = ReadLockTypes.None, BehaviorsOnBlocked behaviorsOnBlocked = BehaviorsOnBlocked.Wait)
        {
            if (null == connection)
            {
                using (var connection2 = GetDbConnection())
                {
                    return await InternalSelectEntitiesAsync(connection2, null, readLock, behaviorsOnBlocked);
                }
            }
            return await InternalSelectEntitiesAsync(connection, transaction, readLock, behaviorsOnBlocked);
        }
        private TEntity[] InternalSelectEntities(
            IDbConnection connection, IDbTransaction transaction,
            ReadLockTypes readLock = ReadLockTypes.None, BehaviorsOnBlocked behaviorsOnBlocked = BehaviorsOnBlocked.Wait)
        {
            var (sql, parameters) = EntityDescriptor.GenerateSelectEntitiesSqlAndParameters(
                readLock, behaviorsOnBlocked);
            var entities = connection.Query<TEntity>(sql, parameters, transaction);
            return entities.ToArray();
        }
        private async Task<TEntity[]> InternalSelectEntitiesAsync(
            IDbConnection connection, IDbTransaction transaction,
            ReadLockTypes readLock = ReadLockTypes.None, BehaviorsOnBlocked behaviorsOnBlocked = BehaviorsOnBlocked.Wait)
        {
            var (sql, parameters) = EntityDescriptor.GenerateSelectEntitiesSqlAndParameters(
                readLock, behaviorsOnBlocked);
            var entities = await connection.QueryAsync<TEntity>(sql, parameters, transaction);
            return entities.ToArray();
        }

        public int InsertOrUpdateEntity(TEntity entity,
            IDbConnection connection = null, IDbTransaction transaction = null)
        {
            if (null == connection)
            {
                using (var connection2 = GetDbConnection())
                {
                    return InternalInsertOrUpdateEntities(new[] { entity }, connection2, null);
                }
            }
            return InternalInsertOrUpdateEntities(new[] { entity }, connection, transaction);
        }
        public async Task<int> InsertOrUpdateEntityAsync(TEntity entity,
            IDbConnection connection = null, IDbTransaction transaction = null)
        {
            if (null == connection)
            {
                using (var connection2 = GetDbConnection())
                {
                    return await InternalInsertOrUpdateEntitiesAsync(new[] { entity }, connection2, null);
                }
            }
            return await InternalInsertOrUpdateEntitiesAsync(new[] { entity }, connection, transaction);
        }
        public int InsertOrUpdateEntities(IEnumerable<TEntity> entities,
            IDbConnection connection = null, IDbTransaction transaction = null)
        {
            if (null == connection)
            {
                using (var connection2 = GetDbConnection())
                {
                    return InternalInsertOrUpdateEntities(entities, connection2, null);
                }
            }
            return InternalInsertOrUpdateEntities(entities, connection, transaction);
        }
        public async Task<int> InsertOrUpdateEntitiesAsync(IEnumerable<TEntity> entities,
            IDbConnection connection = null, IDbTransaction transaction = null)
        {
            if (null == connection)
            {
                using (var connection2 = GetDbConnection())
                {
                    return await InternalInsertOrUpdateEntitiesAsync(entities, connection2, null);
                }
            }
            return await InternalInsertOrUpdateEntitiesAsync(entities, connection, transaction);
        }
        private int InternalInsertOrUpdateEntities(IEnumerable<TEntity> entities,
            IDbConnection connection, IDbTransaction transaction)
        {
            var (sql, parameters) = EntityDescriptor.GenerateInsertOrUpdateSqlAndParameters(entities);
            var affected = connection.Execute(sql, parameters, transaction);
            return affected;
        }
        private async Task<int> InternalInsertOrUpdateEntitiesAsync(IEnumerable<TEntity> entities,
            IDbConnection connection, IDbTransaction transaction)
        {
            var (sql, parameters) = EntityDescriptor.GenerateInsertOrUpdateSqlAndParameters(entities);
            var affected = await connection.ExecuteAsync(sql, parameters, transaction);
            return affected;
        }

        public bool DeleteEntityByPrimaryKey(TEntity key,
            IDbConnection connection = null, IDbTransaction transaction = null)
        {
            if (null == connection)
            {
                using (var connection2 = GetDbConnection())
                {
                    return InternalDeleteEntityByPrimaryKey(key, connection2, null);
                }
            }
            return InternalDeleteEntityByPrimaryKey(key, connection, transaction);
        }
        public async Task<bool> DeleteEntityByPrimaryKeyAsync(TEntity key,
            IDbConnection connection = null, IDbTransaction transaction = null)
        {
            if (null == connection)
            {
                using (var connection2 = GetDbConnection())
                {
                    return await InternalDeleteEntityByPrimaryKeyAsync(key, connection2, null);
                }
            }
            return await InternalDeleteEntityByPrimaryKeyAsync(key, connection, transaction);
        }
        public int DeleteEntitiesByPrimaryKeys(IEnumerable<TEntity> keys,
            IDbConnection connection = null, IDbTransaction transaction = null)
        {
            if (null == connection)
            {
                using (var connection2 = GetDbConnection())
                {
                    return InternalDeleteEntitiesByPrimaryKeys(keys, connection2, null);
                }
            }
            return InternalDeleteEntitiesByPrimaryKeys(keys, connection, transaction);
        }
        public async Task<int> DeleteEntitiesByPrimaryKeysAsync(IEnumerable<TEntity> keys,
            IDbConnection connection = null, IDbTransaction transaction = null)
        {
            if (null == connection)
            {
                using (var connection2 = GetDbConnection())
                {
                    return await InternalDeleteEntitiesByPrimaryKeysAsync(keys, connection2, null);
                }
            }
            return await InternalDeleteEntitiesByPrimaryKeysAsync(keys, connection, transaction);
        }
        private bool InternalDeleteEntityByPrimaryKey(TEntity key,
            IDbConnection connection, IDbTransaction transaction)
        {
            var affected = InternalDeleteEntitiesByPrimaryKeys(new[] { key },
                connection, transaction);
            return 1 == affected;
        }
        private async Task<bool> InternalDeleteEntityByPrimaryKeyAsync(TEntity key,
            IDbConnection connection, IDbTransaction transaction)
        {
            var affected = await InternalDeleteEntitiesByPrimaryKeysAsync(new[] { key },
                connection, transaction);
            return 1 == affected;
        }
        private int InternalDeleteEntitiesByPrimaryKeys(IEnumerable<TEntity> keys,
            IDbConnection connection, IDbTransaction transaction)
        {
            var (sql, parameters) = EntityDescriptor.GenerateDeleteEntitiesByPrimaryKeysSqlAndParameters(keys);
            var affected = connection.Execute(sql, parameters, transaction);
            return affected;
        }
        private async Task<int> InternalDeleteEntitiesByPrimaryKeysAsync(IEnumerable<TEntity> keys,
            IDbConnection connection, IDbTransaction transaction)
        {
            var (sql, parameters) = EntityDescriptor.GenerateDeleteEntitiesByPrimaryKeysSqlAndParameters(keys);
            var affected = await connection.ExecuteAsync(sql, parameters, transaction);
            return affected;
        }

        object INiubilityRepository.SelectEntityByPrimaryKey(object key,
            IDbConnection connection, IDbTransaction transaction,
            ReadLockTypes readLock, BehaviorsOnBlocked behaviorsOnBlocked)
        {
            if (key is TEntity)
            {
                return SelectEntityByPrimaryKey((TEntity)key, connection, transaction, readLock, behaviorsOnBlocked);
            }
            return null;
        }
        async Task<object> INiubilityRepository.SelectEntityByPrimaryKeyAsync(object key,
            IDbConnection connection, IDbTransaction transaction,
            ReadLockTypes readLock, BehaviorsOnBlocked behaviorsOnBlocked)
        {
            if (key is TEntity)
            {
                return await SelectEntityByPrimaryKeyAsync((TEntity)key, connection, transaction, readLock, behaviorsOnBlocked);
            }
            return null;
        }
        object[] INiubilityRepository.SelectEntitiesByPrimaryKeys(IEnumerable<object> keys,
            IDbConnection connection, IDbTransaction transaction,
            ReadLockTypes readLock, BehaviorsOnBlocked behaviorsOnBlocked)
        {
            var typedKeys = keys?.OfType<TEntity>();
            var entities = SelectEntitiesByPrimaryKeys(typedKeys, connection, transaction, readLock, behaviorsOnBlocked);
            return entities?.Cast<object>()?.ToArray();
        }
        async Task<object[]> INiubilityRepository.SelectEntitiesByPrimaryKeysAsync(IEnumerable<object> keys,
            IDbConnection connection, IDbTransaction transaction,
            ReadLockTypes readLock, BehaviorsOnBlocked behaviorsOnBlocked)
        {
            var typedKeys = keys?.OfType<TEntity>();
            var entities = await SelectEntitiesByPrimaryKeysAsync(typedKeys, connection, transaction, readLock, behaviorsOnBlocked);
            return entities?.Cast<object>()?.ToArray();
        }
        object[] INiubilityRepository.SelectEntities(
            IDbConnection connection, IDbTransaction transaction,
            ReadLockTypes readLock, BehaviorsOnBlocked behaviorsOnBlocked)
        {
            var entities = SelectEntities(connection, transaction, readLock, behaviorsOnBlocked);
            return entities?.Cast<object>()?.ToArray();
        }
        async Task<object[]> INiubilityRepository.SelectEntitiesAsync(
            IDbConnection connection, IDbTransaction transaction,
            ReadLockTypes readLock, BehaviorsOnBlocked behaviorsOnBlocked)
        {
            var entities = await SelectEntitiesAsync(connection, transaction, readLock, behaviorsOnBlocked);
            return entities?.Cast<object>()?.ToArray();
        }
        int INiubilityRepository.InsertOrUpdateEntity(object entity,
            IDbConnection connection, IDbTransaction transaction)
        {
            if (entity is TEntity)
            {
                return InsertOrUpdateEntity((TEntity)entity, connection, transaction);
            }
            return 0;
        }
        async Task<int> INiubilityRepository.InsertOrUpdateEntityAsync(object entity,
            IDbConnection connection, IDbTransaction transaction)
        {
            if (entity is TEntity)
            {
                return await InsertOrUpdateEntityAsync((TEntity)entity, connection, transaction);
            }
            return 0;
        }
        int INiubilityRepository.InsertOrUpdateEntities(IEnumerable<object> entities,
            IDbConnection connection, IDbTransaction transaction)
        {
            var typedEntities = entities?.OfType<TEntity>();
            return InsertOrUpdateEntities(typedEntities, connection, transaction);
        }
        Task<int> INiubilityRepository.InsertOrUpdateEntitiesAsync(IEnumerable<object> entities,
            IDbConnection connection, IDbTransaction transaction)
        {
            var typedEntities = entities?.OfType<TEntity>();
            return InsertOrUpdateEntitiesAsync(typedEntities, connection, transaction);
        }
        bool INiubilityRepository.DeleteEntityByPrimaryKey(object key,
            IDbConnection connection, IDbTransaction transaction)
        {
            if (key is TEntity)
            {
                return DeleteEntityByPrimaryKey((TEntity)key, connection, transaction);
            }
            return false;
        }
        async Task<bool> INiubilityRepository.DeleteEntityByPrimaryKeyAsync(object key,
            IDbConnection connection, IDbTransaction transaction)
        {
            if (key is TEntity)
            {
                return await DeleteEntityByPrimaryKeyAsync((TEntity)key, connection, transaction);
            }
            return false;
        }
        int INiubilityRepository.DeleteEntitiesByPrimaryKeys(IEnumerable<object> keys,
            IDbConnection connection, IDbTransaction transaction)
        {
            var typedKeys = keys?.OfType<TEntity>();
            return DeleteEntitiesByPrimaryKeys(typedKeys, connection, transaction);
        }
        Task<int> INiubilityRepository.DeleteEntitiesByPrimaryKeysAsync(IEnumerable<object> keys,
            IDbConnection connection, IDbTransaction transaction)
        {
            var typedKeys = keys?.OfType<TEntity>();
            return DeleteEntitiesByPrimaryKeysAsync(typedKeys, connection, transaction);
        }
    }
}