﻿using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace Niubility.Database
{
    public interface INiubilityRepository
    {
        object SelectEntityByPrimaryKey(object key,
            IDbConnection connection = null, IDbTransaction transaction = null,
            ReadLockTypes readLock = ReadLockTypes.None, BehaviorsOnBlocked behaviorsOnBlocked = BehaviorsOnBlocked.Wait);
        Task<object> SelectEntityByPrimaryKeyAsync(object key,
            IDbConnection connection = null, IDbTransaction transaction = null,
            ReadLockTypes readLock = ReadLockTypes.None, BehaviorsOnBlocked behaviorsOnBlocked = BehaviorsOnBlocked.Wait);

        object[] SelectEntitiesByPrimaryKeys(IEnumerable<object> keys,
            IDbConnection connection = null, IDbTransaction transaction = null,
            ReadLockTypes readLock = ReadLockTypes.None, BehaviorsOnBlocked behaviorsOnBlocked = BehaviorsOnBlocked.Wait);
        Task<object[]> SelectEntitiesByPrimaryKeysAsync(IEnumerable<object> keys,
            IDbConnection connection = null, IDbTransaction transaction = null,
            ReadLockTypes readLock = ReadLockTypes.None, BehaviorsOnBlocked behaviorsOnBlocked = BehaviorsOnBlocked.Wait);

        object[] SelectEntities(
            IDbConnection connection = null, IDbTransaction transaction = null,
            ReadLockTypes readLock = ReadLockTypes.None, BehaviorsOnBlocked behaviorsOnBlocked = BehaviorsOnBlocked.Wait);
        Task<object[]> SelectEntitiesAsync(
            IDbConnection connection = null, IDbTransaction transaction = null,
            ReadLockTypes readLock = ReadLockTypes.None, BehaviorsOnBlocked behaviorsOnBlocked = BehaviorsOnBlocked.Wait);

        int InsertOrUpdateEntity(object entity,
            IDbConnection connection = null, IDbTransaction transaction = null);
        Task<int> InsertOrUpdateEntityAsync(object entity,
            IDbConnection connection = null, IDbTransaction transaction = null);
        int InsertOrUpdateEntities(IEnumerable<object> entities,
            IDbConnection connection = null, IDbTransaction transaction = null);
        Task<int> InsertOrUpdateEntitiesAsync(IEnumerable<object> entities,
            IDbConnection connection = null, IDbTransaction transaction = null);

        bool DeleteEntityByPrimaryKey(object key,
            IDbConnection connection = null, IDbTransaction transaction = null);
        Task<bool> DeleteEntityByPrimaryKeyAsync(object key,
            IDbConnection connection = null, IDbTransaction transaction = null);
        int DeleteEntitiesByPrimaryKeys(IEnumerable<object> keys,
            IDbConnection connection = null, IDbTransaction transaction = null);
        Task<int> DeleteEntitiesByPrimaryKeysAsync(IEnumerable<object> keys,
            IDbConnection connection = null, IDbTransaction transaction = null);
    }
}