﻿using System;
using System.Threading;

namespace Niubility.Database.Common
{
    //// to avoid adding another dependency
    //// borrowed from https://github.com/Humanizr/Humanizer/blob/main/src/Humanizer/Inflections/Vocabularies.cs
    internal static class Vocabularies
    {
        private static Lazy<Vocabulary> Singleton;

        static Vocabularies()
        {
            Singleton = new Lazy<Vocabulary>(BuildDefault, LazyThreadSafetyMode.PublicationOnly);
        }

        /// <summary>
        /// The default vocabulary used for singular/plural irregularities.
        /// Rules can be added to this vocabulary and will be picked up by called to Singularize() and Pluralize().
        /// At this time, multiple vocabularies and removing existing rules are not supported.
        /// </summary>
        public static Vocabulary Default { get => Singleton.Value; }

        private static Vocabulary BuildDefault()
        {
            var plurals = new[] {
                ("$", "s"),
                ("s$", "s"),
                ("(ax|test)is$", "$1es"),
                ("(octop|vir|alumn|fung)us$", "$1i"),
                ("(alias|status)$", "$1es"),
                ("(bu)s$", "$1ses"),
                ("(buffal|tomat|volcan)o$", "$1oes"),
                ("([ti])um$", "$1a"),
                ("sis$", "ses"),
                ("(?:([^f])fe|([lr])f)$", "$1$2ves"),
                ("(hive)$", "$1s"),
                ("([^aeiouy]|qu)y$", "$1ies"),
                ("(x|ch|ss|sh)$", "$1es"),
                ("(matr|vert|ind)ix|ex$", "$1ices"),
                ("([m|l])ouse$", "$1ice"),
                ("^(ox)$", "$1en"),
                ("(quiz)$", "$1zes"),
                ("(campus)$", "$1es"),
                ("^is$", "are")
                //// additional
            };
            var singulars = new[] {
                ("s$", string.Empty),
                ("(n)ews$", "$1ews"),
                ("([ti])a$", "$1um"),
                ("((a)naly|(b)a|(d)iagno|(p)arenthe|(p)rogno|(s)ynop|(t)he)ses$", "$1$2sis"),
                ("(^analy)ses$", "$1sis"),
                ("([^f])ves$", "$1fe"),
                ("(hive)s$", "$1"),
                ("(tive)s$", "$1"),
                ("([lr])ves$", "$1f"),
                ("([^aeiouy]|qu)ies$", "$1y"),
                ("(s)eries$", "$1eries"),
                ("(m)ovies$", "$1ovie"),
                ("(x|ch|ss|sh)es$", "$1"),
                ("([m|l])ice$", "$1ouse"),
                ("(bus)es$", "$1"),
                ("(o)es$", "$1"),
                ("(shoe)s$", "$1"),
                ("(cris|ax|test)es$", "$1is"),
                ("(octop|vir|alumn|fung)i$", "$1us"),
                ("(alias|status)es$", "$1"),
                ("^(ox)en", "$1"),
                ("(vert|ind)ices$", "$1ex"),
                ("(matr)ices$", "$1ix"),
                ("(quiz)zes$", "$1"),
                ("(campus)es$", "$1"),
                ("^are$", "is"),
                //// additional
                ("(goods)$", "$1"),
                ("(class)$", "$1")
            };
            var irregulars = new[] {
                ("person", "people"),
                ("man", "men"),
                ("child", "children"),
                ("sex", "sexes"),
                ("move", "moves"),
                ("goose", "geese"),
                ("alumna", "alumnae"),
                ("criterion", "criteria"),
                ("wave", "waves")
            };
            var uncountables = new[] {
                "equipment",
                "information",
                "rice",
                "money",
                "species",
                "series",
                "fish",
                "sheep",
                "deer",
                "aircraft",
                "oz",
                "tsp",
                "tbsp",
                "ml",
                "l",
                "status"
            };

            return new Vocabulary(plurals, singulars, irregulars, uncountables);
        }
    }
}