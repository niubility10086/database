﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;

namespace Niubility.Database.Common
{
    public class Replacer
    {
        private readonly Rule[] Rules;

        public Replacer(IEnumerable<KeyValuePair<string, string>> rules)
        {
            Rules = rules.Select(r => new Rule(r.Key, r.Value)).ToArray();
        }
        public Replacer(params KeyValuePair<string, string>[] rules)
            : this((IEnumerable<KeyValuePair<string, string>>)rules)
        { }
        public Replacer(IEnumerable<(string, string)> rules)
        {
            Rules = rules.Select(r => new Rule(r.Item1, r.Item2)).ToArray();
        }
        public Replacer(params (string, string)[] rules)
            : this((IEnumerable<(string, string)>)rules)
        { }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public string ApplyMatchedRule(string word)
        {
            string text = word;
            int num = 0;
            int num2 = Rules.Length - 1;
            while (num2 >= num && (text = Rules[num2].Apply(word)) == null)
            {
                num2--;
            }

            return text;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public string ApplyAllRules(string word)
        {
            if (null == word)
            {
                return null;
            }

            foreach (var rule in Rules)
            {
                word = rule.Apply(word) ?? word;
            }
            return word;
        }


        private class Rule
        {
            private readonly Regex Regex;
            private readonly string Replacement;

            public Rule(string pattern, string replacement)
            {
                Regex = new Regex(pattern, RegexOptions.IgnoreCase | RegexOptions.Compiled);
                Replacement = replacement;
            }

            public string Apply(string word)
            {
                if (!Regex.IsMatch(word))
                {
                    return null;
                }

                return Regex.Replace(word, Replacement);
            }
        }
    }
}