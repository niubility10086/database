﻿namespace Niubility.Database
{
    public enum BehaviorsOnBlocked
    {
        Wait = 0,
        /// <summary>
        /// SqlServer:  WITH (NOWAIT)
        /// MySql:      {FOR UPDATE OF t} NOWAIT
        /// </summary>
        NoWait = 1,
        /// <summary>
        /// SqlServer:  WITH (READPAST)
        /// MySql:      {FOR UPDATE OF t} SKIP LOCKED
        /// </summary>
        SkipLocked = 2
    }
}