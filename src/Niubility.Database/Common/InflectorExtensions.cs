﻿using System.Text;
using System.Text.RegularExpressions;

namespace Niubility.Database.Common
{
    //// to avoid adding another dependency
    //// borrowed from https://github.com/Humanizr/Humanizer/blob/main/src/Humanizer/InflectorExtensions.cs
    public static class InflectorExtensions
    {
        //
        // Summary:
        //     Pluralizes the provided input considering irregular words
        //
        // Parameters:
        //   word:
        //     Word to be pluralized
        //
        //   inputIsKnownToBeSingular:
        //     Normally you call Pluralize on singular words; but if you're unsure call it with
        //     false
        public static string Pluralize(this string word, bool inputIsKnownToBeSingular = true)
        {
            var result = new StringBuilder();
            var matches = Regex.Matches(word, "(?:^|_| +)(.)");
            for (var i = 0; i < matches.Count; i++)
            {
                var match = matches[i];
                if ((i + 1) < matches.Count)
                {
                    result.Append(word.Substring(match.Index, (matches[i + 1].Index - match.Index)));
                }
                else
                {
                    var swap = word.Substring(match.Index);
                    var d = match.Groups[0].Length - match.Groups[1].Length;
                    if (d > 0)
                    {
                        result.Append(swap.Substring(0, d));
                        swap = swap.Substring(d);
                    }
                    result.Append(Vocabularies.Default.Pluralize(swap, inputIsKnownToBeSingular));
                }
            }

            return result.ToString();
            //return Vocabularies.Default.Pluralize(word, inputIsKnownToBeSingular);
        }

        //
        // Summary:
        //     Singularizes the provided input considering irregular words
        //
        // Parameters:
        //   word:
        //     Word to be singularized
        //
        //   inputIsKnownToBePlural:
        //     Normally you call Singularize on plural words; but if you're unsure call it with
        //     false
        public static string Singularize(this string word, bool inputIsKnownToBePlural = true)
        {
            var result = new StringBuilder();
            var matches = Regex.Matches(word, "(?:^|_| +)(.)");
            for (var i = 0; i < matches.Count; i++)
            {
                var match = matches[i];
                if ((i + 1) < matches.Count)
                {
                    result.Append(word.Substring(match.Index, (matches[i + 1].Index - match.Index)));
                }
                else
                {
                    var swap = word.Substring(match.Index);
                    var d = match.Groups[0].Length - match.Groups[1].Length;
                    if (d > 0)
                    {
                        result.Append(swap.Substring(0, d));
                        swap = swap.Substring(d);
                    }
                    result.Append(Vocabularies.Default.Singularize(swap, inputIsKnownToBePlural));
                }
            }

            return result.ToString();
            //return Vocabularies.Default.Singularize(word, inputIsKnownToBePlural);
        }

        //
        // Summary:
        //     By default, pascalize converts strings to UpperCamelCase also removing underscores
        //
        //
        // Parameters:
        //   input:
        public static string Pascalize(this string input)
        {
            return Regex.Replace(input, "(?:^|_| +)(.)", (Match match) => match.Groups[1].Value.ToUpper());
        }

        //
        // Summary:
        //     Same as Pascalize except that the first character is lower case
        //
        // Parameters:
        //   input:
        public static string Camelize(this string input)
        {
            string text = input.Pascalize();
            if (text.Length <= 0)
            {
                return text;
            }

            return text.Substring(0, 1).ToLower() + text.Substring(1);
        }

        //
        // Summary:
        //     Separates the input words with underscore
        //
        // Parameters:
        //   input:
        //     The string to be underscored
        public static string Underscore(this string input)
        {
            return Regex.Replace(Regex.Replace(Regex.Replace(input, "([\\p{Lu}]+)([\\p{Lu}][\\p{Ll}])", "$1_$2"), "([\\p{Ll}\\d])([\\p{Lu}])", "$1_$2"), "[-\\s]", "_").ToLower();
        }

        //
        // Summary:
        //     Replaces underscores with dashes in the string
        //
        // Parameters:
        //   underscoredWord:
        public static string Dasherize(this string underscoredWord)
        {
            return underscoredWord.Replace('_', '-');
        }

        //
        // Summary:
        //     Replaces underscores with hyphens in the string
        //
        // Parameters:
        //   underscoredWord:
        public static string Hyphenate(this string underscoredWord)
        {
            return underscoredWord.Dasherize();
        }

        //
        // Summary:
        //     Separates the input words with hyphens and all the words are converted to lowercase
        //
        //
        // Parameters:
        //   input:
        public static string Kebaberize(this string input)
        {
            return input.Underscore().Dasherize();
        }
    }
}