﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Niubility.Database.Common
{
    //// to avoid adding another dependency
    //// borrowed from https://github.com/Humanizr/Humanizer/blob/main/src/Humanizer/Inflections/Vocabulary.cs
    internal class Vocabulary
    {
        private readonly Replacer Plurals;
        private readonly Replacer Singulars;
        private readonly string[] Uncountables;

        internal Vocabulary(
            IEnumerable<(string, string)> plurals,
            IEnumerable<(string, string)> singulars,
            IEnumerable<(string, string)> irregulars,
            IEnumerable<string> uncountables)
        {
            plurals = plurals.Union(irregulars.Select(ir => (
                $"({ir.Item1}){ir.Item1.Substring(1)}$",
                $"$1{ir.Item2.Substring(1)}"
            )));
            singulars = singulars.Union(irregulars.Select(ir => (
                $"({ir.Item2}){ir.Item2.Substring(1)}$",
                $"$1{ir.Item1.Substring(1)}"
            )));

            Plurals = new Replacer(plurals);
            Singulars = new Replacer(singulars);
            Uncountables = uncountables.ToArray();
        }

        //
        // Summary:
        //     Pluralizes the provided input considering irregular words
        //
        // Parameters:
        //   word:
        //     Word to be pluralized
        //
        //   inputIsKnownToBeSingular:
        //     Normally you call Pluralize on singular words; but if you're unsure call it with
        //     false
        public string Pluralize(string word, bool inputIsKnownToBeSingular = true)
        {
            string text = ApplyRules(Plurals, word);
            if (inputIsKnownToBeSingular)
            {
                return text ?? word;
            }

            string text2 = ApplyRules(Singulars, word);
            string text3 = ApplyRules(Plurals, text2);
            if (text2 != null && text2 != word && text2 + "s" != word && text3 == word && text != word)
            {
                return word;
            }

            return text;
        }

        //
        // Summary:
        //     Singularizes the provided input considering irregular words
        //
        // Parameters:
        //   word:
        //     Word to be singularized
        //
        //   inputIsKnownToBePlural:
        //     Normally you call Singularize on plural words; but if you're unsure call it with
        //     false
        //
        //   skipSimpleWords:
        //     Skip singularizing single words that have an 's' on the end
        public string Singularize(string word, bool inputIsKnownToBePlural = true)
        {
            string text = ApplyRules(Singulars, word);
            if (inputIsKnownToBePlural)
            {
                return text ?? word;
            }

            string text2 = ApplyRules(Plurals, word);
            string text3 = ApplyRules(Singulars, text2);
            if (text2 != word && word + "s" != text2 && text3 == word && text != word)
            {
                return word;
            }

            return text ?? word;
        }

        private string ApplyRules(Replacer replacer, string word)
        {
            if (word == null)
            {
                return null;
            }

            if (word.Length < 1)
            {
                return word;
            }

            if (IsUncountable(word))
            {
                return word;
            }

            string text = replacer.ApplyMatchedRule(word);
            if (text == null)
            {
                return text;
            }

            return MatchUpperCase(word, text);
        }

        private bool IsUncountable(string word)
        {
            return Uncountables.Contains(word.ToLower());
        }

        private string MatchUpperCase(string word, string replacement)
        {
            if (!char.IsUpper(word[0]) || !char.IsLower(replacement[0]))
            {
                return replacement;
            }

            return char.ToUpper(replacement[0]) + replacement.Substring(1);
        }
    }
}