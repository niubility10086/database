﻿using System.Data;

namespace Niubility.Database
{
    public interface IDatabaseConnectionManager
    {
        IDbConnection GetDbConnection();
    }
}