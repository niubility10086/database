﻿namespace Niubility.Database
{
    public enum ReadLockTypes
    {
        None = 0,
        NoLock = -1,
        /// <summary>
        /// SqlServer:  WITH (ROWLOCK)
        /// MySql:      FOR SHARE
        /// </summary>
        Share = 1,
        /// <summary>
        /// SqlServer:  WITH (UPDLOCK)
        /// MySql:      FOR UPDATE
        /// </summary>
        Update = 2
    }
}