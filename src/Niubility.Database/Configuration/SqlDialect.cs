﻿namespace Niubility.Database
{
    public enum SqlDialect
    {
        /// <summary>
        /// SQLite
        /// </summary>
        SqLite = 0,
        /// <summary>
        /// MS SQL Server
        /// </summary>
        SqlServer = 1,
        /// <summary>
        /// MySql
        /// </summary>
        MySql = 2
    }
}