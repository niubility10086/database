﻿using Niubility.Database.Entity;
using System;
using System.Collections.Concurrent;
using System.Runtime.CompilerServices;

namespace Niubility.Database
{
    public static class OrmConfiguration
    {
        private static volatile SqlDialect _SqlDialect = SqlDialect.SqLite;
        private static readonly ConcurrentDictionary<Type, EntityDescriptor> EntityDescriptorCache =
            new ConcurrentDictionary<Type, EntityDescriptor>();

        public static SqlDialect SqlDialect
        {
            get => _SqlDialect;
            set => _SqlDialect = value;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        internal static EntityDescriptor GetEntityDescriptor<TEntity>()
        {
            var entityType = typeof(TEntity);
            if (EntityDescriptorCache.TryGetValue(entityType, out var descriptor))
            {
                return descriptor;
            }
            else
            {
                descriptor = EntityDescriptor.GenerateEntityDescriptor<TEntity>(_SqlDialect);
                return EntityDescriptorCache.GetOrAdd(typeof(TEntity), descriptor);
            }
        }
    }
}