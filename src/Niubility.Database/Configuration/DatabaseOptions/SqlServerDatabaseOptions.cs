﻿using Niubility.Database.Common;

namespace Niubility.Database.Configuration
{
    public class SqlServerDatabaseOptions : DatabaseOptions
    {
        private static readonly Replacer QUOTEDIDENTIFIERRESOLVER_SQLSERVER = new Replacer(
            (@"^((?'S'\[)?(?<name>.+))*(?'E-S'\])?$(?(S)(?!))", "${name}"),
            (@"\]", @"]]"),
            (@"^.*$", @"[${0}]")
        );

        public SqlServerDatabaseOptions()
        {
            IsUsingSchemas = true;
            QuotedIdentifierResolver = QUOTEDIDENTIFIERRESOLVER_SQLSERVER;
            ParameterPrefix = "@";
        }
    }
}