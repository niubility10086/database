﻿using Niubility.Database.Common;

namespace Niubility.Database.Configuration
{
    public class MySqlDatabaseOptions : DatabaseOptions
    {
        private static readonly Replacer QUOTEDIDENTIFIERRESOLVER_MYSQL = new Replacer(
            (@"^((?'S'`)?(?<name>.+))*(?'E-S'`)?$(?(S)(?!))", "${name}"),
            (@"`", @"``"),
            (@"^.*$", @"`${0}`")
        );

        public MySqlDatabaseOptions()
        {
            IsUsingSchemas = false;
            QuotedIdentifierResolver = QUOTEDIDENTIFIERRESOLVER_MYSQL;
            ParameterPrefix = "@";
        }
    }
}