﻿using Niubility.Database.Common;
using System.Runtime.CompilerServices;

namespace Niubility.Database.Configuration
{
    public class DatabaseOptions
    {
        private static readonly Replacer QUOTEDIDENTIFIERRESOLVER_ANSI = new Replacer(
            (@"^((?'S'"")?(?<name>.+))*(?'E-S'"")?$(?(S)(?!))", "${name}"),
            (@"""", @""""""),
            (@"^.*$", @"""${0}""")
        );

        public DatabaseOptions()
        {
            IsUsingSchemas = false;
            QuotedIdentifierResolver = QUOTEDIDENTIFIERRESOLVER_ANSI;
            ParameterPrefix = "@";
        }

        public bool IsUsingSchemas { get; protected set; }

        protected Replacer QuotedIdentifierResolver;
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public string QuoteIdentifier(string identifier)
        {
            return QuotedIdentifierResolver.ApplyAllRules(identifier);
        }

        public string ParameterPrefix { get; protected set; }
        public string ParameterSuffix { get; protected set; }
    }
}