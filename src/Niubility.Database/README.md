﻿### Install NuGet Package
```shell
> dotnet add package Niubility.Database
```

### Create entity class file
```C#
using Niubility.Database.Entity;

namespace {YourProject}
{
    [Table("dbo", "SystemConfigurations")]
    public class SystemConfigurations
    {
        [Column(false, false, false, true, 200, null, 1, null)]
        public string Key { get; set; }
        [Column(false, false, false, false, 2000, null, 2, null)]
        public string Value { get; set; }
    }
}
```

### Create repository class file
```C#
using Niubility.Database;

namespace {YourProject}
{
    private class SystemConfigurationRepository : NiubilityRepository<SystemConfigurations>
    {
        public SystemConfigurationRepository(IDatabaseConnectionManager connectionManager)
            : base(connectionManager)
        { }

        ...append your methods here
    }
}
```

### Then, you can easily CRUD for your tables
```C#
var repository = new SystemConfigurationRepository(connectionManager);
//// get all rows
repository.SelectEntities(connection, transaction);
//// get data row by key
repository.SelectEntityByPrimaryKey(key, connection, transaction);
//// get rows matched keys
repository.SelectEntitiesByPrimaryKeys(keys, connection, transaction);
//// insert or update entity
repository.InsertOrUpdateEntity(entity, connection, transaction);
//// insert or update entities
repository.InsertOrUpdateEntities(entities, connection, transaction);
//// delete data row by key
repository.DeleteEntityByPrimaryKey(key, connection, transaction);
//// delete rows matched keys
repository.DeleteEntitiesByPrimaryKeys(keys, connection, transaction);
```